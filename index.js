#!/usr/bin/env node --harmony
'use strict';

const program = require('commander'),
      colors = require('colors'),
      glob = require('glob'),
      fs = require('fs'),
      promiseDelay = require('promise-delay'),
      translate = require('google-translate-api'),
      xlsx = require('node-xlsx')

program
  .version('1.0.0')
  .option('-d, --directory [directory]', 'Working directory', __dirname)
  .option('-o, --output [directory]', 'Output directory', './output')
  .parse(process.argv)

glob(`${program.directory}/*.@(xls|xlsx)`, (err, files) => {
  if(err) {
    console.log('Error: No such directory'.red)
    process.exit(1)
  }

  Logic(files)
})

function Logic(files) {
  files.forEach((filepath) => {

    const excelFile = xlsx.parse(filepath);

    var promises = [];

    // for(let i=0; i < excelFile.length; i++) {
    //   let sheet = excelFile[i];
    //   let p = TranslateSheet(sheet);
    //   p.then((res) => {
    //     return res
    //   })
    //   promises.push(p)
    // }
    RecursiveSheet(excelFile, 0, promises)
  })
}

function RecursiveSheet(excelFile, index, promises) {
  if(typeof excelFile[index] == 'undefined') {
    Promise.all(promises).then((res) => {
      console.log('Building'.green)
      const buffer = xlsx.build(res),
            outputPath = `${program.directory}/output`
      fs.exists(outputPath, exists => {
        if (!exists) {
          fs.mkdirSync(outputPath)
        }

        fs.writeFile(`${outputPath}/result.xls`, buffer, err => {
          if(err) {
            console.log(`Error (Build): ${err}`.red)
            process.exit(2)
          }
        })
      })
    })

    return;
  }

  var p = TranslateSheet(excelFile[index])
  p.then((res) => {
    RecursiveSheet(excelFile, ++index, promises)
    return res
  })

  promises.push(p);
}

function TranslateSheet(sheet) {
  var ei;

  switch(sheet.name) {
    case 'C_LANG_FORM':
    case 'C_LANG_MESSAGE':
    case 'C_LANG_TOOLTIP': ei = 4; break;
    case 'C_SYS_Option_D_List': ei = 6; break;
    case 'C_SYS_Common_D':
    case 'P_PTC_ProtocolOption_List': ei = 5; break;
  }

  if(typeof ei == 'undefined') {
    console.log('Error: Sheet is not defined'.red)
    process.exit(2);
  }

  var pi = ei + 8,
      data = sheet.data,
      promises = [];

  for(let i=1; i < data.length;i++) {
    let p;
    if(typeof data[i][ei] == 'undefined') {
      
      p = Promise.resolve('').then(() => {
        return([''])
      })
    } else {

      p = new Promise((resolve, reject) => {

        setTimeout(() => {
          console.log(`Start request for fuck Google on sheet "${sheet.name}" #${i}`.yellow);
          translate(data[i][ei], {from: 'en', to: 'fa'})
            .then(res => {
              console.log(`Request on sheet "${sheet.name}" #${i} ended`.blue)
              resolve([res.text])
            })
            .catch(err => {
              console.error(`Error (translate): ${err}`.red)
              reject(err)
            })
        }, i*400)
      })
    }

    promises.push(p)
  }

  return Promise.all(promises).then((res) => {
    // console.log(res);
    console.log(res);
    return {name: sheet.name, data: res};
  });
}